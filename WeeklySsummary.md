### git
1.`git remote add origin` xxxxxx 链接到远程分支xxxxxx（git上面的仓库地址）

2.`git remote -v` 查看是否链接到了远端。

3.`git pull origin master` 加载git仓库到本地

4.`git add -A` 暂存文件

5.`git commit -m` "我们提交的内容"

6.`git push origin master` 最后推送到master分支。

### 分支操作
`git status` 查看当前所在分支和更改文件

`git branch` 查看所有分支

`git branch xxx` 新建新分支

`git checkout xxx` 切换到xxx分支

`git checkout -b xxx` 创建并切换到新分支

`git pull origin xxx` 拉取xxx分支

`git push origin xxx` 推送xxx分支

`git merge xxx` 合并分支 

`git stash `暂存 (可以查看某分支的状态)

`git stash pop `恢复、


### 项目基础建设

1、封装 git hooks

-下载依赖包  npm i husky

-执行  husky install  生成.husky文件夹

-添加钩子（hook）   `npx husky add .husky/commit-msg "npx --no-install commitlint --edit"`
·`commit-msg git commit -m ""`

*下载依赖

commitlint  语法

@commitlint/cli  cli指令

@commitlint/config-conventional  规则

添加 commitlint.config.js 配置校验规则  module.exports = {
  `extends: ['@commitlint/config-conventional']`
};

2、eslint

下载依赖 lint-staged

在 scripts 字段添加lint运行 lint-staged指令

监听文件变化

添加`git hooks pre-commit` `  git add`

`npx husky add .husky/pre-commit "npm run lint"`

husky 生成钩子、npx husky add .husky/钩子的名称 执行的命令





### axios二次封装
1、统一管理接口地址

2、接口的二次复用

ajax的作用是发送http请求    是window对象 提供 XMLHttpRequests 对象

用于浏览器和服务器进行通信

then的响应值取决于拦截器的返回值





### 虚拟dom

虚拟dom优点

减少dom的开销  真实dom携带的属性过多 直接操作dom会引起重绘、回流

可以跨平台开发 js对象可以运行在不同平台  而dom只能运行在浏览器